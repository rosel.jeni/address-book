## Address-Book

This is a simple Address Book created as a practice with Facebook's Flux architecture and using React for UI components.

## Quickstart

The project dependencies are not included in this repository. To install all the dependencies you will need NPM. Run this command in the main directory:

```bash
npm install
```

Also run this to create bundle.js with browserify, it will update the file everytime you make a change in your JavaScript files.

```bash
npm start
```
